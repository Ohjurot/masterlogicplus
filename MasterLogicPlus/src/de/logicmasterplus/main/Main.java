package de.logicmasterplus.main;

import test.logicmasterplus.main.TestMain;

/***
 * Default Main Class
 * @author Ludwig Fuechsl - ludwig@fuechsl.org
 * @version 19.06.2017
 */
public class Main {

	/***
	 * Main Program Access Point
	 * @param args Command Args <br>Info:<br><ul><li>Debug - Start Debug Manager</li></ul>
	 * @author Ludwig Fuechsl
	 */
	public static void main(String[] args) {
		System.out.println("Master Logic Plus wird geladen....");
		//Register Shutdown Hook
		Runtime.getRuntime().addShutdownHook(ShutdownHook.makeThread());
		
		if(args.length > 0){
			switch (args[0]) {
			case "test":
				//Load Debug Class
				if(args.length > 1) TestMain.runLoad(args[1]);
				else TestMain.runTest();
				break;
			default:
				System.out.println("Argument \"" + args[0] + "\" nicht erkannt");
				break;
			}
		}else{
			//TODO: Implement Game
		}
	}

}
