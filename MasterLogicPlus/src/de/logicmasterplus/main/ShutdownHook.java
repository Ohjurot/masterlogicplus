package de.logicmasterplus.main;

/***
 * Default Shutdown Hook (Executed on Shutdown)
 * @author Ludwig Fuechsl - ludwig@fuechsl.org
 * @version 19.06.2017
 */
public class ShutdownHook implements Runnable{

	/***
	 * This will be Executed on Shutdown
	 * @author Ludwig Fuechsl
	 */
	@Override
	public void run() {
		//TODO: Implement Shutdown Actions
		
		System.out.println("Master Logic Plus erfolgreich beendet");
	}

	/***
	 * make Shutdown Hook from this Class
	 * @author Ludwig Fuechsl
	 * @return Thread
	 */
	public static Thread makeThread(){
		return new Thread(new ShutdownHook());
	}
}
