package test.logicmasterplus.main;

import javax.swing.JOptionPane;

/***
 * Class for Handling GUI and Draw Tests
 * @author Ludwig Fuechsl - ludwig@fuechsl.org
 * @version 19.06.2017
 */
public final class TestMain {
	
	/***
	 * Run GUI for Opening a Draw/GUI Test
	 * @author Ludwig Fuechsl
	 */
	public static void runTest(){
		String testProgramm = JOptionPane.showInputDialog(null, "Bitte geben Sie den Namen des Test Programm ein","Logic Master Plus", JOptionPane.INFORMATION_MESSAGE);
		runLoad(testProgramm);
	}
	
	/***
	 * Load A class direct by String
	 * @param testProgramm Class Package name
	 * @author Ludwig Fuechsl
	 */
	@SuppressWarnings("rawtypes")
	public static void runLoad(String testProgramm){
		if(testProgramm.length() > 0){
			System.out.println("Lade Klasse test.logicmasterplus." + testProgramm + ".TRun");
			try{
				Class c = Class.forName("test.logicmasterplus." + testProgramm + ".TRun");
				TestRun tr = (TestRun)c.newInstance();
				tr.Run();
			}catch (Exception e) {
				System.out.println("Fehler beim Laden der TestKlasse \"" + testProgramm + "\": " + e.getMessage());
			}
		}
	}
}
