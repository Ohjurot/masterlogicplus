package test.logicmasterplus.main;

/***
 * Interface for Handeling test Runs
 * @author Ludwig Fuechsl - ludwig@fuechsl.org
 * @version 19.06.2017
 */
public interface TestRun {
	
	/***
	 * Default Run Function for a TestRun
	 * @author Ludwig Fuechsl
	 */
	public void Run();
}
