package test.logicmasterplus.grid;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import test.logicmasterplus.main.TestRun;

/***
 * Test Class for Testing Grid Draw & And Grid Click
 * @author Ludwig Fuechsl - ludwig@fuechsl.org
 * @version 20.06.2017
 */
public class TRun extends JFrame implements TestRun{
	private static final long serialVersionUID = 4474339277657334980L;
	
	private int gridDimension = 25;
	private int requireGridSizeX = 50;
	private int requireGridSizeY = 50;
	private int gridSizeX = 0;
	private int gridSizeY = 0;
	private Gameobject[][] gameobjects;

	/***
	 * Create Window
	 */
	public TRun() {
		System.out.println("Lade Fenster f�r Grid Test");
		this.setTitle("Logic Master Plus - Grid Test");
		this.setSize(Toolkit.getDefaultToolkit().getScreenSize());
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.addMouseListener(clickListener);
		this.requireGridSizeX = this.requireGridSizeX + 2;
		this.requireGridSizeY = this.requireGridSizeY + 2;
		gameobjects = new Gameobject[this.requireGridSizeX+900][this.requireGridSizeY+900];
		
	}
	
	@Override
	public void Run() {
		System.out.println("Starte Grid Test");
		this.setVisible(true);
	}
	
	@Override
	public void paint(Graphics g) {
		//Draw Grid
        super.paint(g);  // fixes the immediate problem.
        Graphics2D g2 = (Graphics2D) g;
        
        //Resize Array and vars if required
        if(((gridSizeX != ((this.getWidth()/gridDimension) + 1)) || (gridSizeY != ((this.getHeight()/gridDimension) + 1)))){
        	gridDimension = 100;
        	while((gridDimension * requireGridSizeX) > this.getWidth() || (gridDimension * requireGridSizeY) > this.getHeight()){
        		gridDimension--;
        	}
        	if(gridDimension != 0){
        		gridSizeX = (this.getWidth()/gridDimension) + 1;
                gridSizeY = (this.getHeight()/gridDimension) + 1;
                System.out.println("GridSize: " + gridSizeX + "," + gridSizeY);
        	}
        }
        
        for(int i = 0; i < this.getWidth(); i = i + gridDimension){
        	Line2D lin = new Line2D.Double(i,0,i,this.getHeight());
            g2.draw(lin);
        }
        
        for(int i = 0; i < this.getHeight(); i = i + gridDimension){
        	Line2D lin = new Line2D.Double(0,i,this.getWidth(),i);
            g2.draw(lin);
        }
        
        for(int x = 0; x < gridSizeX; x++){
        	for(int y = 0; y < gridSizeY; y++){
        		Gameobject object = gameobjects[x][y];
        		if(object != null){
        			g2.setColor(Color.red);
        			Point2D point = getGridPos(x, y);
        			g2.fillRect((int)point.getX(), (int)point.getY(), gridDimension, gridDimension);
        		}
        	}
        }
    }
	
	/***
	 * Get the Grid Position (X,Y Form position) of a grid node
	 * @param x The x gridnode
	 * @param y The y gridnode
	 * @return Point2D
	 */
	private Point2D getGridPos(final int x, final int y){
		int posX = gridDimension * x;
		int posY = (gridDimension * y) + gridDimension;
		return new Point2D.Double(posX,posY);
	}
	
	private void createObject(int x, int y){
		String color = JOptionPane.showInputDialog(null, "Farbe eingeben","Logic Master Plus", JOptionPane.INFORMATION_MESSAGE);
		String width = JOptionPane.showInputDialog(null, "Gr��e width eingeben:","Logic Master Plus", JOptionPane.INFORMATION_MESSAGE);
		String height = JOptionPane.showInputDialog(null, "Gr��e height eingeben:","Logic Master Plus", JOptionPane.INFORMATION_MESSAGE);
		
		Gameobject object = new Gameobject(Integer.parseInt(width), Integer.parseInt(height), color);
		gameobjects[x][y] = object;
		this.repaint();
	}

	//Mouse Listener
	private MouseListener clickListener = new MouseListener() {
		@Override
		public void mouseClicked(MouseEvent e) {
			int gridX = e.getX()/gridDimension;
			int gridY = (e.getY()/gridDimension) - 1;
			System.out.println("X: " + e.getX() + ", Y: " + e.getY() + " GRID: " + gridX + "," + gridY);
			
			Point2D pos = getGridPos(gridX, gridY);
			Gameobject object = gameobjects[gridX][gridY];
			System.out.println("Gameobject at [" + gridX + "," + gridY + "] -> Position: " + pos.getX() + "," + pos.getY());
			
			if(object != null){
				System.out.println("Object: [" + object.getWidth() + "," + object.getHeight() + "] Color: " + object.getColor() );
			}else{
				System.out.println("Object: NULL -> Create one");
				createObject(gridX, gridY);
			}
		}
		
		@Override
		public void mouseEntered(MouseEvent e) {}
		@Override
		public void mouseExited(MouseEvent e) {}
		@Override
		public void mousePressed(MouseEvent e) {}
		@Override
		public void mouseReleased(MouseEvent e) {}
	};
}
