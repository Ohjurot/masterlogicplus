package test.logicmasterplus.grid;

/***
 * Test Gameobject (Colored Blocks)
 * @author Ludwig Fuechsl - ludwig@fuechsl.org
 * @version 20.06.2017
 */
public class Gameobject {
	private int width;
	private int height;
	private String color;
	
	/***
	 * Default Constructor
	 * @param width Width (Grid Node)
	 * @param height Height (Grid Node)
	 * @param color Color String
	 */
	public Gameobject(int width, int height, String color) {
		this.width = width;
		this.height = height;
		this.color = color;
	}

	/***
	 * Get the Width
	 * @return Int
	 */
	public int getWidth() {
		return width;
	}

	/***
	 * Get the Height
	 * @return Int
	 */
	public int getHeight() {
		return height;
	}

	/***
	 * Get the Color
	 * @return String
	 */
	public String getColor() {
		return color;
	}
}
