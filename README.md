# README #

## INFO: Das Spiel ist noch in Entwicklung (Änderungen vorbehalten)##

Willkommen auf der Quellcode Seite von MasterLogicPlus folgende Informationen finden sie in dieser Readme Datei:

* Kurzbeschreibung des Spiels

* Wichtige Informationen

* Download & Lizenz Infos

* Das Entwickler Team

* Handbuch des Spiels

* Kann ich mitentwickeln?

* Support

### Kurzbeschriebung ###

Im Spiel MasterLogicPlus geht es darum, ein Rätsel in möglichst schneller Zeit zu lösen. Zusätzlich können eigene Rätzel erstellt werden und mit Freuden (oder anderen Nutzern) geteilt werden. Durch eine einfach zu erlernende Skript Sprache können eigene Inhalte (Logik Elemente) erstellt werden. 

### Wichtige Informationen ###

Vorausetzungen:

* Rechner mit JRE8

* Empfolen: 64bit

* Weitere vorausetzungen folgen

Rechtliches: Das Spiel MasterLogicPlus wurde von Ludwig Füchsl erfunden und Programmiert. Bei allen Veröffentlichungen von Inhalten des Spiels, muss der Name des Spiels, die Webseite (alternativ: Bitbucket url) angegeben werden.
Beispiel:
~~~~
in diesem Video gezeigt: MasterLogicPlus
Kostenloser Download: https://bitbucket.org/Ohjurot/masterlogicplus/
~~~~

### Download & Lizenes ###

Aktuell ist das Spiel in Entwicklung, um die DEV Version des Spieles auszuproben, benötigen Sie das JDK und eine Java Entwicklungsumgebung mit Git Unterstützung. Über die Entwicklungsumgebung können sie den Quellcode downloaden und ausprobieren. 

Veröffentlicht unter [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/) (Quellcode Änderungen / Eigene Versionen)

Inhalte unter [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/) (Erstellte Inhalte z.B.: Rätzel oder Logik Elemente dürfen unter beliebiger Lizenz weiter gegeben werden)

### Entwickler Team ###

Aktuell besteht unser Team aus folgenden Mitgliedern:

* Ludwig Füchsl: Team & Entwicklungs Leiter, Head-DEV

### Handbuch ###

Das Handbuch wird veröffentlicht sobald es eine erste BETA-Version des Spiels gibt.

### Kann ich mitentwickeln ###

Aktuell suchen wir keine Entwickler in unserem DEV Team. Sie können jedoch einen Privaten Fork des Projektes erstellen, und Ihre eigene Modifikation des Projektes entwickeln.

### Support ###

Für persönlichen Support (Frage, Antwort, etc.) besuchen Sie bitte unsere Webseite [MasterLogicPlus](http://logic.technikthemod.de).
Um Fehler zu melden benutzen Sie bitte die Bitbucket Funktion Issues